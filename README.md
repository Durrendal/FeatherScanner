# What?
A NodeMCU Lua based version of Lucidiot's WifiScanner. They both use the ESP8266 chip, so they work more or less the same exact way. This one is just in Lua instead of C.

WifiScanner: https://tildegit.org/lucidiot/wifiscan.git

# Why?
WarWalking/Driving is fun?

# References:
ESP8266 Wifi Types:
https://github.com/espressif/ESP8266_RTOS_SDK/blob/0f200b46/components/esp8266/include/esp_wifi_types.h#L57-L67

NodeMCU Lua Source:
https://github.com/nodemcu/nodemcu-firmware.git

# Installation:
## Getting Firmware:
Navigate to the nodemcu builder:
https://nodemcu-build.com/

Select the following options then build:
Channel: release
Modules: bit, file, GPIO, net, node SJSON, timer, UART, WIFI, WIFI monitor, WPS

You could build the firmware yourself also. I'll add steps for that in a bit.

## Flashing Firmware:
### Using esptool.py
esptool is packaged for Alpine, but you can grab it here if you need to: https://github.com/espressif/esptool
```
esptool.py --port <serial-port-of-ESP8266> write_flash -fm <flash-mode> 0x00000 <nodemcu-firmware>.bin
```

For the Feather (esp8266) you need to use qio as your flash mode (that's 512kb modules)
For ESP32's you'd use dio (>=4MB modules)
And for ESP8285 would use dout.
You didn't really need to know those last two, but there's a difference and it's important to use the right one.

Here's an example of what I've done in the past: 
```
esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --port /dev/ttyUSB0 write_flash -fm qio 0x00000 nodemcu-release-10-modules-2021-10-23-01-29-09-integer.bin
```

Or just
```
make reset
make flash
```

## Adding the Software:
```
make build
```
This will invoke nodemcu-tool to upload the lua files in src/.

There are esperbuilds for nodemcu-tool and luatool at https://gitlab.com/durrendal/esper.git in the /repo directory if you'd care to use that.

Alternately you could try and invoke make build_old to use luatool, however when I've tried to use this recently it has failed to upload to my feather and soft bricked it, I've had to erase and then reflash it to get it to work afterwards.

## Gambit
There's a script to invoke all three steps of the make process in order, just be sure to reseat the cable going to your feather before running the script.
```
scripts/gambit.sh
```

## With Platformio
Honestly, when I tried to use this it didn't work, so I'll update this with working instructions when I figure it out.

# Using FeatherScan
## Listening:
Once flashed you can connect to the feather with screen, or any other serial program of your choice
```
sudo screen /dev/ttyUSB0 115200
```
there's a simple connect script in the scripts/ directory which can be configured to log the data that the feather scanner outputs.

This is where I'll put the information about the logging daemon I plan to write.
```
cogito ergo sum
```

## RPIZW Installation:
Instructions for soldering the board to a Raspbery Pi ZW will go here in the near future.

# Attribution
Feather icon made by Markus from the Noun Project
