#!/bin/sh
#Connect to a feather scanner & log output if enabled
baud=115200
dev=/dev/ttyUSB0
logging="true"
logfile=$(date +%Y-%m-%d_%H:%M:%S).log

if [ "$logging" == "true" ]; then
	echo "Serial with logging.."
	sleep 3
	sudo screen -L -Logfile $logfile $dev $baud
else
	echo "Serial without logging.."
	sudo screen $dev $baud
fi
