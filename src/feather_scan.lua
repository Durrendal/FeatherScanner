--Replace cjson with sjson
_G.cjson = sjson
--Set eps8266 into station mode
wifi.setmode(wifi.STATION)
--Enable 802.11b mode (170ma draw, 11mbs, +17dBm) _G and _N (802.11g/n) are also supported, but have less range.
wifi.setphymode(wifi.PHYMODE_B)
--Set red LED on
gpio.mode(3, gpio.OUTPUT)

--We should scan for hidden networks, and all scan all channels
scan_cfg = {
   show_hidden = 1,
   channel = 0,
}

--https://github.com/espressif/ESP8266_RTOS_SDK/blob/0f200b46/components/esp8266/include/esp_wifi_types.h#L57-L67
auth_types = {
   ["0"] = "OPEN",
   ["1"] = "WEP",
   ["2"] = "WPA_PSK",
   ["3"] = "WPA2_PSK",
   ["4"] = "WPA_WPA2_PSK",
   ["5"] = "WPA2_ENT",
   ["6"] = "WPA3_PSK",
   ["7"] = "WPA2_WPA3_PSK",
}

mode = "running"
scan = true

function list_aps(t)
   --forcefully flush the timer, if something is broken.
   --tmr.wdclr()
   --Try to scan again if we haven't found anything useful
   if type(t) ~= "table" then
	  scan = true
	  return
   end
   --If we scan data, transform it into json
   for ssid,v in pairs(t) do
	  local ssid_tbl = {
		 ["SSID"] = "",
		 ["CHANNEL"] = "",
		 ["RSSI"] = "",
		 ["AUTH"] = "",
		 ["BAND"] = "",
		 ["BSSID"] = "",
		 ["SECLVL"] = "",
	  }
	  authtyp, rssi, bssid, band, channel = string.match(v, "(%d),(-?%d+),(%x%x:%x%x:%x%x:%x%x:%x%x:%x%x),(%d+)")
	  --If we get Open, WEP, or WPA consider it insecure
	  if authtyp >= "0" and authtyp <= "2" then
		 ssid_tbl["SECLVL"] = "Insecure"
		 ssid_tbl["AUTH"] = auth_types[authtyp]
	  --If we get WPA2, WPA2, or WPA2 Ent in any form, consider it secure
	  elseif authtyp >= "3" and authtyp <= "7" then
		 ssid_tbl["SECLVL"] = "Secure"
		 ssid_tbl["AUTH"] = auth_types[authtyp]
	  --If something else crops up, something is horribly wrong, but we should record it anyways.
	  else
		 ssid_tbl["SECLVL"] = "Unknown AuthType!"
		 ssid_tbl["AUTH"] = authtyp
	  end
	  ssid_tbl["SSID"] = ssid
	  ssid_tbl["CHANNEL"] = channel
	  ssid_tbl["RSSI"] = tonumber(rssi)
	  ssid_tbl["BAND"] = tonumber(band)
	  ssid_tbl["BSSID"] = bssid
	  print(cjson.encode(ssid_tbl))
   end
   scan = true
end

--register a timer that cycles every 10 seconds and runs the scanner
loop = tmr.create()
loop:register(10000, tmr.ALARM_AUTO, function ()
				 if scan then
					scan = false
					--Blink on
					gpio.write(3, gpio.LOW)
					tmr.delay(1000000)
					--Scan all the things
					wifi.sta.getap(list_aps)
					--Blink off
					gpio.write(3, gpio.HIGH)
					tmr.delay(1000000)
				 end
end
)
loop:start()
