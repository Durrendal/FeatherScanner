package main

import (
	//"github.com/stratoberry/go-gpsd"
	_ "github.com/mattn/go-sqlite3"
	"github.com/joho/godotenv"
	"github.com/tarm/serial"
	"encoding/json"
	"path/filepath"
	"database/sql"
	//"reflect"
	"strconv"
	//"time"
	//"fmt"
	"log"
	"os"
	"io"
)

//dump serial json to file

//Map Feather Scanner output to struct
type netinfo struct {
	SECLVL string `json:SECLVL`
	BSSID string `json:BSSID`
	BAND int `json:BAND`
	RSSI int `json:RSSI`
	SSID string `json:SSID`
	AUTH string `json:AUTH` 
}

//Change from godotenv to yaml parsed config?
//func configure() {
//}

//I let Matt Daemon handle all of my Daemon processes.
func mattDaemon() {
	//Set /dev and baud from config, if /etc/featherscan isn't found, try and load daemon.conf in the CWD
	if _, err := os.Stat("/etc/featherscan/"); !os.IsNotExist(err) {
		godotenv.Load(filepath.Join("/etc/featherscan/", "daemon.conf"))
	} else {
		godotenv.Load("daemon.conf")
	}

	//Convert baud str to int
	baud_rate, _ := strconv.Atoi(os.Getenv("BAUD"))

	//Create serial configuration for the feather scanner
	feather := &serial.Config{
		Name: os.Getenv("DEVICE"),
		Baud: baud_rate,
	}

	//Open the serial port
	ser, err := serial.OpenPort(feather)
	if err != nil {
		log.Fatal(err)
	}

	//If the DB doesn't exist create it
	if _, err := os.Stat(os.Getenv("DB_PATH")); err != nil {
		os.Create(os.Getenv("DB_PATH"))
	}

	//Open a connection to the database
	db, err := sql.Open(os.Getenv("DB_TYPE"), os.Getenv("DB_PATH"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	
	rows, _ := db.Query("select 1 from networks;")
	if rows == nil {
		schema := `CREATE TABLE networks (
			bssid text primary key not null,
			seclvl text not null,
			band integer not null,
			rssi integer not null,
			ssid text not null,
			auth text not null,
			lon real null,
			lat real null,
			dt datetime default current_timestamp,
			check ((lat is null and lon is null) or (lat is not null and lon is not null)),
			check (lon is null or (lon > -180 and lon < 180)),
			check (lat is null or (lat > -90 and lat < 90)),
			check (rssi < 0),
			check (band > 0));`
		stmt, err := db.Prepare(schema)
		if err != nil {
			log.Fatal(err)
		}
		stmt.Exec()
	}

	//Create an IO buffer, read each line in the buffer and flush to logging
	buf := make([]byte, 512)
	for {
		n, err := ser.Read(buf)
		if err == io.EOF {
			log.Fatal(err)
		}

		//Maybe make this less static, break out the various network types into their own tables
		//Divest SQL functions from MattDaemon and pass around the serial/sql configurations as objects
		scan := netinfo{}
		json.Unmarshal(buf[:n], &scan)
		//(seclvl, bssid, band, rssi, ssid, auth, long, lat) //need to add a GPS monitor here too
		statement, err := db.Prepare("INSERT INTO networks(bssid, seclvl, band, rssi, ssid, auth) values(?,?,?,?,?,?)")
		_, err = statement.Exec(scan.BSSID, scan.SECLVL, scan.BAND, scan.RSSI, scan.SSID, scan.AUTH)
		//log.Print(string(buf[:n]))
	}
}

func main() {
	mattDaemon()
}
