module feather_daemon

go 1.17

require (
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
)
