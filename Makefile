ESPER_REPO ?= ~/Development/Esper/repo
#FIRMWARE ?= firmware/nodemcu-dev-integer.bin
FIRMWARE ?= firmware/nodemcu-dev-float.bin
FLASH_MODE ?= qio
DEV_DEVICE ?= /dev/ttyUSB0

prepare:
	luarocks-5.3 install luaserial
	cd $(ESPER_REPO) && esper luatool.esper
	cd $(ESPER_REPO) && esper nodemcu-tool.esper

reset:
	esptool.py --port $(DEV_DEVICE) erase_flash

flash:
	esptool.py --port $(DEV_DEVICE) write_flash -fm $(FLASH_MODE) 0x00000 $(FIRMWARE)

build:
	nodemcu-tool upload src/init.lua
	nodemcu-tool upload src/feather_scan.lua

build_old:
	luatool -p /dev/ttyUSB0 -f src/init.lua -v
